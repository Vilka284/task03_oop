package com.tack3_oop.controller;

import com.tack3_oop.model.Plane;

import java.util.ArrayList;

/**
 * Controller Interface
 */
public interface Controller {

    /**
     * @param planeArrayList set list of planes
     */
    void setPlaneArrayList(ArrayList<Plane> planeArrayList);

    /**
     * @return list of planes
     */
    ArrayList<Plane>  getPlaneArrayList();
}
