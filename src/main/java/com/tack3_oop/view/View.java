package com.tack3_oop.view;

import com.tack3_oop.controller.Controller;
import com.tack3_oop.controller.ControllerImpl;
import com.tack3_oop.model.Plane;

import java.util.*;

/**
 * View -> Controller </br>
 * View <- Controller </br>
 */
public class View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scanner = new Scanner(System.in);

    /**
     * View constructor - menu
     */
    public View(){
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Check planes");
        menu.put("2", "  2 - Add plane");
        menu.put("3", "  3 - Sort by flight distance");
        menu.put("4", "  4 - Find by fuel tank capacity");
        menu.put("Q", "  Q - quit program");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);

        String name = "1";
        int fuel = 1;
        int distance = 1;
        int weight = 1;
        ArrayList<Plane> tempArrayList = controller.getPlaneArrayList();
        tempArrayList.add(new Plane(name, fuel, distance, weight));
        controller.setPlaneArrayList(tempArrayList);
    }

    /**
     * Get list of all planes
     */
    private void pressButton1(){
        controller.getPlaneArrayList();
    }

    /**
     * Create new plane
     */
    private void pressButton2(){
        String name;
        int fuel;
        int distance;
        int weight;
        System.out.println("Name: ");
        name = scanner.nextLine();
        System.out.println("Capacity of fuel tank: ");
        fuel = scanner.nextInt();
        System.out.println("Flight distance: ");
        distance = scanner.nextInt();
        System.out.println("lift weight: ");
        weight = scanner.nextInt();
        ArrayList<Plane> tempArrayList = controller.getPlaneArrayList();
        tempArrayList.add(new Plane(name, fuel, distance, weight));
        controller.setPlaneArrayList(tempArrayList);
    }

    /**
     * Sort planes by flight distance
     */
    private void pressButton3(){
        ArrayList<Plane> planeArrayList = controller.getPlaneArrayList();
        planeArrayList.sort(Comparator.comparing(Plane::getFlightDistance));
        System.out.println("Planes and their flight values");
        for (Plane p:
                planeArrayList) {
            System.out.println(p.getName() + " <-> " + p.getFlightDistance());
        }
    }

    /**
     * Show planes that suit to you by fuel tank capacity
     */
    private void pressButton4(){
        ArrayList<Plane> planeArrayList = controller.getPlaneArrayList();
        System.out.println("Input min: ");
        int min = scanner.nextInt();
        System.out.println("Input max: ");
        int max = scanner.nextInt();
        int f;
        System.out.println("There are planes that applied to you:");
        for (Plane p:
                planeArrayList) {
            f = p.getFuelCapacity();
            if (f >= min && f <= max){
                System.out.println(p.getName() + " <-> " + f);
            }
        }
    }

    /**
     * Show menu
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * Get choice from user
     */
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
