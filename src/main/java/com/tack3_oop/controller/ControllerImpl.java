package com.tack3_oop.controller;

import com.tack3_oop.model.BusinessLogic;
import com.tack3_oop.model.Model;
import com.tack3_oop.model.Plane;

import java.util.ArrayList;

/**
 * Controller <- Model </br>
 * Controller -> Model </br>
 * Controller <- View </br>
 * Controller -> View </br>
 */
public class ControllerImpl implements Controller {
    private Model model;

    /**
     * Constructor to create new model
     */
    public ControllerImpl() {
        model = new BusinessLogic();
    }

    /**
     * @param planeArrayList set list of planes
     */
    @Override
    public void setPlaneArrayList(ArrayList<Plane> planeArrayList) {
        model.setPlaneArrayList(planeArrayList);
    }

    /**
     * @return list of planes
     */
    @Override
    public ArrayList<Plane> getPlaneArrayList() {
        return model.getPlaneArrayList();
    }
}
