package com.tack3_oop.model;

import java.util.ArrayList;

/**
 * Abstract class Plane to build any kind of planes
 */
public class Plane {
    private  String name;
    private int fuelCapacity;
    private int flightDistance;
    private int liftWeight;

    public Plane(String name, int fuelCapacity, int flightDistance, int liftWeight) {
        this.name = name;
        this.fuelCapacity = fuelCapacity;
        this.flightDistance = flightDistance;
        this.liftWeight = liftWeight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFuelCapacity() {
        return fuelCapacity;
    }

    public void setFuelCapacity(int fuelCapacity) {
        this.fuelCapacity = fuelCapacity;
    }

    public int getFlightDistance() {
        return flightDistance;
    }

    public void setFlightDistance(int flightDistance) {
        this.flightDistance = flightDistance;
    }

    public int getLiftWeight() {
        return liftWeight;
    }

    public void setLiftWeight(int liftWeight) {
        this.liftWeight = liftWeight;
    }
}
