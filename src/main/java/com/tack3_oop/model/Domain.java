package com.tack3_oop.model;

import java.util.ArrayList;

/**
 * Domain
 */
public class Domain {

    private ArrayList<Plane> planeArrayList;

    /**
     * Constructor to create new list of planes
     */
    public Domain() {
        planeArrayList = new ArrayList<>();
    }

    /**
     * @return planes list
     */
    public ArrayList<Plane> getPlaneArrayList() {
        return planeArrayList;
    }

    /**
     * @param planeArrayList set planes list
     */
    public void setPlaneArrayList(ArrayList<Plane> planeArrayList) {
        this.planeArrayList = planeArrayList;
    }
}
