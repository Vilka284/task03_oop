package com.tack3_oop.model;

import java.util.ArrayList;

/**
 * Model Interface
 */
public interface Model {

    /**
     * @return planes list
     */
    ArrayList<Plane> getPlaneArrayList();

    /**
     * @param planeArrayList set planes list
     */
    void setPlaneArrayList(ArrayList<Plane> planeArrayList);
}
