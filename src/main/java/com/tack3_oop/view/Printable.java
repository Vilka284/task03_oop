package com.tack3_oop.view;

/**
 * Printable Interface
 */
@FunctionalInterface
public interface Printable {

    void print();
}
